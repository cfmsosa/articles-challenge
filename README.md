
Articles Challenge

# Articles Challenge
> REIGN Full Stack Developer


██ ABOUT ███████████████████████████████████████████████████████████████████


## Getting Started
Articles Challenge includes 2 diferent proyects


### challenge-api
-	requires :
	node v15.14.0
	MongoDB running 

-	NPM install
```
npm install
```
-	Run as developer
```
npm run start:dev
```
-	Run as production distibution
```
npm run start:prod
```

>> Articles database is populated automatically on app run and once an hour while running


### challenge-client
-	requires :
	node v16.4.2

-	NPM install
```
npm install
```

-	Run as developer
```
npm run watch
```
-	Run as production distibution
```
npm run start
```





## Author

* **Carlos Sosa** - *cfmsosa* - [linkedin](https://www.linkedin.com/in/carlos-moncada-sosa-404820168/)

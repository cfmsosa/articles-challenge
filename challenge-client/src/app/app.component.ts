import { Component, OnInit } from '@angular/core';
import { ArticlesService } from './articles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'challenge-client';
  public listArticles:any = []

  constructor(private articlesService:ArticlesService){}

  ngOnInit(): void {
    this.loadData();
  }

  public loadData(){
    this.articlesService.get('http://localhost:3000/articles').subscribe(data => {
      var map: { [key: string]: any } = data;
      var todaysDate = new Date();
      var yesterdayDate = new Date();
      yesterdayDate.setDate(todaysDate.getDate() - 1);
      for (const thisone of Object.keys(map)) {
        let day = new Date(map[thisone].date);
        if(day.setHours(0,0,0,0) == todaysDate.setHours(0,0,0,0)) {
          map[thisone].disdate = new Date(map[thisone].date).toTimeString().split(' ')[0];
        } else if(day.setHours(0,0,0,0) == yesterdayDate.setHours(0,0,0,0)) {
          map[thisone].disdate = 'Yesterday';
        } else {
          let strdate = new Date(map[thisone].date).toString().split(' ');
          map[thisone].disdate = strdate[1] + ' ' + strdate[2];
          
        }
        map[thisone].date = new Date(map[thisone].date);
      }
      map.sort(function(a:any,b:any){
        return b.date - a.date;
      });


      this.listArticles = map;
    })
  }
}

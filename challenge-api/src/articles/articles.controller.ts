import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Post()
  create(@Body() createArticleDto: CreateArticleDto) {
    return this.articlesService.create(createArticleDto);
  }

  @Get()
  findAll() {
    return this.articlesService.findAll();
  }

  @Get(':title')
  findOne(@Param('title') title: string) {
    return this.articlesService.findOne(title);
  }

  @Put(':title')
  update(@Param('title') title: string, @Body() updateArticleDto: UpdateArticleDto) {
    return this.articlesService.update(title, updateArticleDto);
  }

  @Delete(':title')
  remove(@Param('title') title: string) {
    return this.articlesService.remove(title);
  }
}

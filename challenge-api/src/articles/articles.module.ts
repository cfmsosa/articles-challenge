import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { HttpModule } from '@nestjs/axios';

import { Article, ArticleSchema } from 'src/schemas/article.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Article.name , schema: ArticleSchema }]),HttpModule],
  controllers: [ArticlesController],
  providers: [ArticlesService],
})
export class ArticlesModule {}
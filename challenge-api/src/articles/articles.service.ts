import { Model } from 'mongoose';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from 'src/schemas/article.schema';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { HttpService } from '@nestjs/axios';
import { Cron, CronExpression } from '@nestjs/schedule';


@Injectable()
export class TasksService {

}


@Injectable()
export class ArticlesService implements OnModuleInit {
  
  constructor(@InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
    private httpService: HttpService
    ) {}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const createdArticle = new this.articleModel(createArticleDto);
    return createdArticle.save();
  }

  async findAll(): Promise<Article[]> {
    return this.articleModel.find().exec();
  }

  async findOne(name: string) {
    return this.articleModel.findOne({ name });
  }

  async update(name: string, updateArticleDto: UpdateArticleDto) {
    return this.articleModel.updateOne({ name },{ $set: { ...updateArticleDto } });
  }

  async remove(name: string) {
    return this.articleModel.deleteOne({ name });
  }

  async onModuleInit(): Promise<void> {
    this.callArticles();
  }

  @Cron(CronExpression.EVERY_HOUR)
  handleCron() {
    this.callArticles();
  }

  async callArticles() {
    var response = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').toPromise();
    response = response.data.hits;
    let metadata = [];
    var created = 0;

    for (const thisone of Object.keys(response)) {

      if( !(response[thisone].story_title == null && response[thisone].title == null) ){

        let box = {
          name: response[thisone].title ? response[thisone].title : response[thisone].story_title,
          author: response[thisone].author,
          date: response[thisone].created_at,
          url: response[thisone].story_url,
        };

        const mine = await this.articleModel.find({ name: box.name, }).limit(1).select({ name: 1 }).exec();

        if (mine.length == 0) {
          metadata.push( box );
          const createdArticle = new this.articleModel(box);
          createdArticle.save();
          created++;
        }
      }
    }
    console.log( 'new articles : ' + created );
  }

}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  name: string;

  @Prop()
  author: string;
  
  @Prop()
  date: string;
  
  @Prop()
  url: string;  
}

export const ArticleSchema = SchemaFactory.createForClass(Article);